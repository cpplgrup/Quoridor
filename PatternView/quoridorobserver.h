/**
 * @file quoridorobserver.h
 * @brief Definition of class Quoridor::QuoridorObserver.
 */
#ifndef BOARDOBSERVER
#define BOARDOBSERVER

#include "changeevent.h"

/**
 *@brief namespace for Quoridor
 */
namespace Quoridor {
/**
 * @brief The QuoridorObserver abstract class
 * This class is to be used as model for quoridor observers.
 */
class QuoridorObserver
{
public:
    /**
     * @brief Prints a quoridor game in the desired format.
     * @param event the change event.
     */
    virtual void printGame(ChangeEvent event) = 0;

    /**
     * @brief Notified wen a wall has been placed.
     * @param event the change event.
     */
    virtual void notifyPlaceWall(ChangeEvent event) = 0;

    /**
     * @brief Notified when player has moved.
     * @param event the change event.
     */
    virtual void notifyMovePlayer(ChangeEvent event) = 0;

    /**
     * @brief Notified when player as changed.
     * @param event the change event.
     */
    virtual void notifyChangePlayer(ChangeEvent event) = 0;
};

}//end of Quoridor

#endif // BOARDOBSERVER

