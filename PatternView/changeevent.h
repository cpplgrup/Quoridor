/**
 * @file changeevent.h
 * @brief Definition of class Quoridor::ChangeEvent.
 */
#ifndef CHANGEEVENT_H
#define CHANGEEVENT_H

#include "board.h"
/**
 *@brief namespace for Quoridor
 */
namespace Quoridor {

/**
 * @brief The ChangeEvent class a container of data fired when change on board happens.
 */
class ChangeEvent
{
    const board& source;
    int index, x , y, oldX, oldY;

public:
    /**
     * @brief Creates a change event that contains player index and position of change.
     * @param source the board on wich the change is happening.
     * @param index the index of the player causing this event.
     * @param x the x coordinate of the position affected.
     * @param y the Y coordinate of the position affected.
     * @param oldX the x coordinate before ChangeEvent.
     * @param oldY the y coordinate before ChangeEvent.
     */
    ChangeEvent(const board& source, int index = -1, int x = -1, int y = -1, int oldX = -1, int oldY = -1  );

    /**
     * @brief Gets the board firing the event.
     * @return the board firing the event.
     */
    inline const board& getSource() const;

    /**
     * @brief Gets the index of the player causing the event.
     * @return the index of the player causing the event.
     */
    inline int getIndex() const;

    /**
     * @brief Gets the x coordinates of the position.
     * @return the x coordinate of the position.
     */
    inline int getPosX() const;

    /**
     * @brief Gets the y coordinates of the position.
     * @return the y coordinate of the position.
     */
    inline int getPosY() const;
};

const board & ChangeEvent::getSource() const{
    return source;
}

int ChangeEvent::getIndex() const{
    return index;
}

int ChangeEvent::getPosX() const {
    return x;
}

int ChangeEvent::getPosY() const {
    return y;
}
}//end of Quoridor
#endif // CHANGEEVENT_H
