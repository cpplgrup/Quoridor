/**
 * @file observablequoridor.h
 * @brief Definition of class Quoridor::ObservableQuoridor.
 */
#ifndef OBSERVABLEQUORIDOR
#define OBSERVABLEQUORIDOR

#include <vector>
#include "quoridor.h"
#include "quoridorobserver.h"

/**
 *@brief namespace for Quoridor
 */
namespace Quoridor {

/**
 * @brief The ObservableQuoridor class is use a model for Observable quoridor
 */
class ObservableQuoridor : public quoridor
{
    std::vector<QuoridorObserver*> observers;
    void changePlayer();

public:

    /**
     * @brief Creates a quoridor with the right arguments.
     * @param numPlayers number of players
     * @param size size of board
     * @param names the liste of player names
     * @see quoridor
     */
    ObservableQuoridor(unsigned numPlayers, int size, vector<string> &names);

    /**
     * @brief Registers an observer to monitor of observable quoridor.
     * @param obs the observer to add.
     */
    inline void registerObserver(QuoridorObserver* obs);

    /**
     * @brief Unregisters an observer from monitoring observable quoridor.
     * @param index the index of the observer to unregister.
     */
    inline void unregisterObserver(int index);

    /**
     * @brief Moves the player and automatically notify it's observers of the change.
     * @param newPos the new position of the player.
     * @return true if successful fals if not.
     */
    bool movePlayer(position newPos);

    /**
     * @brief Moves the player and automatically notify it's observers of the change.
     * @param dir the direction in wich player must move.
     * @return true if successful fals if not.
     */
    bool movePlayer(direction& dir);

    /**
     * @brief Places wall and automaically notify it's observers of the change.
     * @param pos position where wall should be placed.
     * @param orientation the orientation of de wall.
     * @return true if successful false if not.
     * @throw logic_error if a player doesn't have path to victory.
     * @see quoridor
     */
    bool placeWall(position pos, direction orientation);
};

void ObservableQuoridor::registerObserver(QuoridorObserver * obs) {
    observers.push_back(obs);
    obs->printGame(ChangeEvent(getBoard(), getCurrentPlayer()));
}

void ObservableQuoridor::unregisterObserver(int index) {
    observers.erase(observers.begin()+index);
}


}//end of Quoridor
#endif // OBSERVABLEQUORIDOR
