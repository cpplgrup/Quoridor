include(../defaults.pri)

TEMPLATE = lib
TARGET = libView
DESTDIR = ../lib


LIBS += -L../lib -llibModel

HEADERS += \
    changeevent.h \
    observablequoridor.h \
    quoridorobserver.h

SOURCES += \
    changeevent.cpp \
    observablequoridor.cpp

