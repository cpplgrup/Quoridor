#include "observablequoridor.h"

using namespace Quoridor;

ObservableQuoridor::ObservableQuoridor(unsigned numPlayers,int size, vector<string> & names) : quoridor (numPlayers,size,names) {

}

bool ObservableQuoridor::placeWall(position pos, direction orientation) {
    if(quoridor::placeWall(pos,orientation)) {
        for(auto obs: observers) obs->notifyPlaceWall(ChangeEvent(getBoard(),getCurrentPlayer(),pos.getX(),pos.getY()));
        changePlayer();
        return true;
    }
    return false;
}

bool ObservableQuoridor::movePlayer(position newPos){
   position oldPos = getBoard().getPlayer(getCurrentPlayer()).getPosition();
   if(quoridor::movePlayer(newPos) ){
        for(auto obs: observers){
            obs->notifyMovePlayer(ChangeEvent(getBoard(),getCurrentPlayer(),newPos.getX(),newPos.getY(),oldPos.getX(),oldPos.getY()));
        }
        changePlayer();
        return true;
    }

    return false;
}

bool ObservableQuoridor::movePlayer(direction& dir){
   position oldPos = getBoard().getPlayer(getCurrentPlayer()).getPosition();
   if(quoridor::movePlayer(dir) ){
       position newPos = getBoard().getPlayer(getCurrentPlayer()).getPosition();
        for(auto obs: observers){
            obs->notifyMovePlayer(ChangeEvent(getBoard(),getCurrentPlayer(),newPos.getX(),newPos.getY(),oldPos.getX(),oldPos.getY()));
        }
        changePlayer();
        return true;
    }

    return false;
}

void ObservableQuoridor::changePlayer() {
    quoridor::changePlayer();
    for (auto o: observers) o->notifyChangePlayer(ChangeEvent(getBoard(), getCurrentPlayer()));
}
