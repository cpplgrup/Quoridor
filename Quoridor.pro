TEMPLATE = subdirs

SUBDIRS += Model \
           PatternView \
           gui\
           Console \
           Tests \

gui.depends =  Model PatternView
Console.depends = Model PatternView
PatternView.depends = Model
Tests.depends = Model


OTHER_FILES += \
    defaults.pri
