#include "player.h"
using namespace Quoridor;

player::player()
{

}

player::player(const std::string &_name, int boardSize, direction origin, int numPlayers, bool ai)
    :name(_name), pos(getStartingPos(origin, boardSize)), origin(origin), nbWall((boardSize + 1) * 2 / numPlayers),ai(ai){}

