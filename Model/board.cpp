#include "board.h"
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <queue>
#include "dude.h"

using namespace std;
using namespace Quoridor;

board::board(unsigned numPlayers , int size, vector<string> names):
    players(std::vector<player *>(numPlayers)), size(size), tiles(size,vector<square>(size)) {
    if (names.size() == numPlayers) for (unsigned i = 0; i < players.size(); i++) {
        Dude d(names[i], size, getDir(i), numPlayers);
        players[i] = &d;
    } else throw std::range_error("Didn't provide right names count! Provided: " + std::to_string(names.size()) + ", required: " + std::to_string(numPlayers));
    initializeBoard();
}

void board::createTiles()
{
    position pos;
    for (int i = 0; i < size; i++) for (int j = 0; j < size; ++j) {
            pos = position(i,j);
            tiles[j][i] = square(pos);
    }
}

void board::setTilesNeighbors() {
    for(int i = 0 ; i <size; i++ ) for (int j = 0; j < size; ++j) {
             if(i - 1 >= 0) tiles[i][j].addNeighbor(direction::north, tiles.at(i-1).at(j));
             if(j + 1 < size) tiles[i][j].addNeighbor(direction::east, tiles.at(i).at(j+1));
             if(i + 1 < size) tiles[i][j].addNeighbor(direction::south, tiles.at(i+1).at(j));
             if(j - 1 >= 0) tiles[i][j].addNeighbor(direction::west, tiles.at(i).at(j-1));
        }
}

void board::initializeBoard() {
    createTiles();
    setTilesNeighbors();
}

int board::getPlayerAtPosition(const position &at) const {
    for (unsigned i = 0; i < players.size(); i++) if (players[i]->getPosition() == at) return i; ;

    return -1;
}

bool board::isPlayerPresent(position at) const {
    for (unsigned i = 0; i < players.size(); i++) if (players[i]->getPosition() == at) return true;

    return false;
}

void board::getPossibleMoves(int pIndex, vector<position>& poss) const {
    player * ref = players[pIndex];
    for (std::pair<direction, const square*> s: tiles[ref->getPosition().getY()][ref->getPosition().getX()].getNeighbors()) {
        if (!isPlayerPresent(s.second->getPosition())) poss.push_back(s.second->getPosition()); // Add neighbors if no player present
        else { // Jamp
            if (s.second->hasNeighbor(s.first) && !isPlayerPresent(s.second->getNeighbors().at(s.first)->getPosition()))
                poss.push_back(s.second->getNeighbors().at(s.first)->getPosition());
            else for (std::pair<direction, const square*> q: s.second->getNeighbors())
                    if (q.first != getOppositeDir(s.first) && !isPlayerPresent(q.second->getPosition())) poss.push_back(q.second->getPosition());
        }
    }
}

bool board::movePlayer(int playerIndex, position newPos) {
    vector<position> poss;
    getPossibleMoves(playerIndex, poss);
    if (std::find(poss.begin(), poss.end(), newPos) != poss.end()) {
        players[playerIndex]->move(newPos);
        return true;
    } else return false;
}

bool board::canPlaceWall(int player, position at, direction orientation) const {
    if (at.getX() > size - 2 || at.getY() > size - 2) return false; // pos can't be < 0, so no double-check here
    if (!players[player]->hasWall()) return false;

//    return topleft.hasNeighbor(isVertical(orientation)? direction::east: direction::south) && botright.hasNeighbor(isVertical(orientation)? direction::west: direction::north)
//        && (topleft.hasNeighbor(isVertical(orientation)? direction::south: direction::east) || botright.hasNeighbor(isVertical(orientation)? direction::north: direction::west));

    const square& topleft = tiles[at.getY()][at.getX()];
    const square& botright = tiles[at.getY() + 1][at.getX() + 1];

    bool overlapCheck = topleft.hasNeighbor(isVertical(orientation)? direction::east: direction::south) && botright.hasNeighbor(isVertical(orientation)? direction::west: direction::north);

    return overlapCheck && centerCheck(at,orientation);
}

 int board::countPieceOfWall(int startX , int startY , int end , int shifting,bool isVertical) const {
     int cpt = 0;
     if(!isVertical){
         while( startY !=  end  && !tiles[startY][startX].hasNeighbor(direction::east)){
             startY +=shifting;
             cpt++;
         }
     }else{
         while( startX !=  end  && !tiles[startY][startX].hasNeighbor(direction::south)){
             startX +=shifting;
             cpt++;
         }
     }
     return cpt;
}

 bool board::centerCheck(position& pos , direction& orientation) const {
    int coord;
    if (!isVertical(orientation)){
        coord = pos.getY()+1;
         if(pos.getY() <= size/2) {
                return countPieceOfWall(pos.getX(),pos.getY(), -1,-1,isVertical(orientation)) % 2 == 0 && countPieceOfWall(pos.getX(),coord,size,+1,isVertical(orientation)) % 2 == 0;
          } else {
             return countPieceOfWall(pos.getX(),coord, size,+1,isVertical(orientation)) % 2 == 0 && countPieceOfWall(pos.getX(),pos.getY(),-1,-1,isVertical(orientation)) % 2 == 0;
         }
    } else {
        coord =pos.getX()+1;
        if(pos.getX() <= size/2) {
               return countPieceOfWall(pos.getX(),pos.getY(), -1,-1,isVertical(orientation)) % 2 == 0 && countPieceOfWall(coord,pos.getY(),size,+1,isVertical(orientation)) % 2 == 0;
         } else {
            return countPieceOfWall(coord,pos.getY(), size,+1,isVertical(orientation)) % 2 == 0 && countPieceOfWall(pos.getX(),pos.getY(),-1,-1,isVertical(orientation)) % 2 == 0;
        }
    }
 }

 void board::undoWall(position& at, direction & orientation)
 {
     square& topleft = tiles[at.getY()][at.getX()];
     square& topRight = tiles[at.getY()][at.getX() + 1];
     square& botLeft = tiles[at.getY() + 1][at.getX()];
     square& botRight = tiles[at.getY() + 1][at.getX() + 1];

     if (isVertical(orientation)) {
         topleft.addNeighbor(direction::east,topRight);
         topRight.addNeighbor(direction::west,topleft);
         botLeft.addNeighbor(direction::east,botRight);
         botRight.addNeighbor(direction::west,botLeft);
     } else {
         topleft.addNeighbor(direction::south,botLeft);
         topRight.addNeighbor(direction::south,botRight);
         botLeft.addNeighbor(direction::north,topleft);
         botRight.addNeighbor(direction::north,topRight);
     }
 }

 void board::setWall(position& at, direction& orientation)
 {
     square& topleft = tiles[at.getY()][at.getX()];
     square& topRight = tiles[at.getY()][at.getX() + 1];
     square& botLeft = tiles[at.getY() + 1][at.getX()];
     square& botRight = tiles[at.getY() + 1][at.getX() + 1];

     if (isVertical(orientation)) {
         topleft.removeNeighbor(direction::east);
         topRight.removeNeighbor(direction::west);
         botLeft.removeNeighbor(direction::east);
         botRight.removeNeighbor(direction::west);
     } else {
         topleft.removeNeighbor(direction::south);
         topRight.removeNeighbor(direction::south);
         botLeft.removeNeighbor(direction::north);
         botRight.removeNeighbor(direction::north);
     }
 }

 bool board::placeWall( int playerIndex,position& at, direction& orientation) {
     if (!canPlaceWall(playerIndex, at, orientation)) return false;
     setWall(at, orientation);
     players[playerIndex]->placeWall();
     if (!playersHavePath()) {
         undoWall(at,orientation);
         return false;
     }

    return true;
}

void board::unmarkSquares () {
    vector<vector<square>>::iterator  row;
    vector<square>::iterator col;
    for (row = tiles.begin(); row != tiles.end(); row++) for (col = row->begin(); col != row->end(); col++)(*col).unMark();
}

bool board::pathFinder(position startPos, direction& origin) {
    queue<square *> searchQueue;//Creates copy of all square good or bad ??

    searchQueue.push(&tiles[startPos.getY()][startPos.getX()]);
    if (searchQueue.back()->isMarked()) cout << "marked!" << endl;
    searchQueue.back()->mark();
    while (!searchQueue.empty()) { //work with element that's in front of the queue
        if (isWinningPosition(searchQueue.front()->getPosition(), origin, size)) {
            unmarkSquares();
            return true;
        }
        for (std::pair<direction, square * const> n: searchQueue.front()->getNeighbors()) {
            square * nei = &tiles[n.second->getPosition().getY()][n.second->getPosition().getX()]; //Reference to the object pointed by square*..
            if (!nei->isMarked()) {
                searchQueue.push(nei);
                searchQueue.back()->mark();
            }
        }
        searchQueue.pop();
    }

    unmarkSquares();
    return false;
}

bool board::playersHavePath() {
    for (player * p: players) if(!pathFinder(p->getPosition(), p->getOrigin())) return false;

    return true;
}

bool board::boardHasWinner() const {
    for (player * ply: players) if(isWinningPosition(ply->getPosition(),ply->getOrigin(), size)) return true;
    return false;
}
