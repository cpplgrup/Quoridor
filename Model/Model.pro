include(../defaults.pri)

TEMPLATE = lib
TARGET = libModel
DESTDIR = ../lib

HEADERS += \
    board.h \
    direction.h \
    gamestate.h \
    player.h \
    position.h \
    quoridor.h \
    square.h \
    quoridorai.h \
    dude.h

SOURCES += \
    board.cpp \
    player.cpp \
    quoridor.cpp \
    square.cpp \
    quoridorai.cpp

