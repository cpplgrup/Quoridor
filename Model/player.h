/**
 * @file player.h
 * @brief Definition of class Quoridor::player.
 */
#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <stdexcept>
#include "position.h"
#include "direction.h"

/**
 *@brief namespace for Quoridor
 */
namespace Quoridor {

/**
 * @brief The player class
 * This class priovides all the characteristics of a quoridor game player.
 */
class player
{
private:
    std::string name;
    position pos;
    direction origin;
    int nbWall;
    bool ai;

public:
    player(); //Required for std::vector<>

    /**
     * @brief Creates a player.
     * @param _name the name of the player.
     * @param boardSize the size of the board he is playing on(useful for calculating nbWall).
     * @param origin his place of origin.
     * @param numPlayers number of players he is playing with.
     * @see direction for infos over origin.
     */
    player(const std::string & _name, int boardSize, direction origin, int numPlayers, bool ai);

    /**
     * @brief Gets the name of the player.
     * @return the name of the player.
     */
    inline const std::string getName() const;

    /**
     * @brief Gets the position of the player.
     * @return the position of the player.
     */
    inline position getPosition() const;

    /**
     * @brief Gets the number of wall the player has.
     * @return the number of wall the player has.
     */
    inline int getNbWall() const;

    /**
     * @brief Gets the origin of the player.
     * @return the origin of the player.
     */
    inline direction& getOrigin();

    /**
     * @brief Checks if a player has wall.
     * @return true if he as fals if not.
     */
    inline bool hasWall() const;

    /**
     * @brief Moves the player to a new position.
     * @param newPos the position where player should move to.
     */
    inline void move(const position& newPos);

    /**
     * @brief Place wall for player..
     * Decreses the number of wal he has.
     */
    inline void placeWall();

    inline bool isAi();
    virtual void makeMove()=0;
};

const std::string player::getName() const {
    return name;
}

position player::getPosition() const {
    return pos;
}

int player::getNbWall() const {
    return nbWall;
}

direction& player::getOrigin() {
    return origin;
}

bool player::hasWall() const {
    return nbWall != 0;
}

void player::move(const position &newPos) {
    pos.setXY(newPos.getX(),newPos.getY());
}

void player::placeWall() {
    --nbWall;
}

bool player::isAi() {
    return ai;
}
inline std::ostream& operator <<(std::ostream& os, player& pl) {  // Required by CATCH
    os << "Player " + pl.getName() + " at ("<< pl.getPosition().getX() << ", " << pl.getPosition().getY() << ") "
        << "from " << pl.getOrigin() << " with " << pl.getNbWall() << " walls.";

    return os;
}

}//end of Quoridor
#endif // PLAYER_H
