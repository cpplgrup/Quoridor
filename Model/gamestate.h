/**
 * @file gamestate.h
 * @brief Definition of enum class Quoridor::GameState.
 */
#ifndef GAMESTATE
#define GAMESTATE

/**
 *@brief namespace for Quoridor
 */
namespace Quoridor {

/**
 * @brief The GameState enum class.
 * The GameState enum class is a strongly typed enum that give the state of a game.
 */
enum class GameState
{
    in_progress,
    draw,
    won,
    quit
};

}//end of Quoridor

#endif // GAMESTATE

