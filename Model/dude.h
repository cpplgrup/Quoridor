#ifndef DUDE
#define DUDE
#include "player.h"

namespace Quoridor {

class Dude: public player
{
public:
    inline Dude(const std::string & _name, int boardSize, direction origin, int numPlayers);
    inline virtual void makeMove();
};

void Dude::makeMove(){

}

Dude::Dude(const std::string & _name, int boardSize, direction origin, int numPlayers):player(_name,boardSize,origin,numPlayers,false) {}


}//end of Quoridor

#endif // DUDE

