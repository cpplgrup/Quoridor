/**
 * @file board.h
 * @brief Definition of class Quoridor::board.
 */
#ifndef BOARD_H
#define BOARD_H

#include <string>
#include <vector>
#include "player.h"
#include "position.h"
#include "square.h"
#include "direction.h"

/**
 *@brief namespace for Quoridor
 */
using namespace std;
namespace Quoridor {

/**
 * @brief The board class
 * This board class based on graphs provides all the characteristics of a normal n x n board while implementing the
 * rules of Quoridor game.
 */
class board
{
private:
    vector<player *> players;
    const int size;
    vector<vector<square>> tiles;

    /**
     * @brief Board initialization.
     * @see board::createTiles()
     * @see board::setTilesNeighbors()
     */
    void initializeBoard();

    /**
     * @brief Creates tiles
     * Creates all the tiles needed to have a n x n board.
     */
    void createTiles();

    /**
     * @brief Set tiles neighbors
     * Give to each tile links to all of his neighbors.
     */
    void setTilesNeighbors();

    /**
     * @brief Calculates if position is center of wall.
     * Does that  by counting the number of piece of all on the left/top and right/buttom
     * @param orientation vertical or horizontal
     * @return true if position is not center of wall false if it is.
     */
    bool centerCheck(position &, direction& orientation) const;

    /**
     * @brief  Counts number of piece of wall from a position to the end.
     * @param startX the starting  x coordinate.
     * @param startY the starting  y coordinate.
     * @param end  the end x or y coordinate for the movement.
     * @param shifting the movement for the search.
     * @param isVertical orientation of seach.
     * @return number of piece wall.
     */
    int countPieceOfWall(int startX, int startY, int end, int shifting,bool isVertical ) const;

    /**
     * @brief Finds path from position to winning position.
     * @param startPos the start position for the search.
     * @param origin
     * @return true if there is path to winning position.
     */
    bool pathFinder(position startPos, direction &origin);

    /**
     * @brief Checks if all players have path to victory.
     * @return true if all players have path and false if not.
     */
    bool playersHavePath();

    /**
     * @brief Unmark all tiles of the board.
     */
    void unmarkSquares ();

    /**
     * @brief Set a wall at given position.
     * @param at the position where wall should be placed.
     * @param orientation the orientation of the wall.
     */
    void setWall(position& at, direction& orientation);

    /**
     * @brief Undo a wall at given position.
     * @param at the position where wall should be removed.
     * @param orientation the orientation of the wall.
     */
    void undoWall(position &at, direction &orientation);

public:
    /**
     * @brief Creates a new board
     * @param numPlayers the number of players in board
     * @param size the size of the board.
     * @param names the names of player present in board.
     * @throw range_error if number of player is deffrent from the number of names recieve.
     */
    board(unsigned numPlayers, int size, vector<std::string> names);
    //~board(); //Sera necessaire pour detruire les square...
    /**
     * @brief Gets the size of board.
     * @return the size of the board.
     */
    inline  int getSize() const;

    /**
     * @brief Gets all possible moves of a player.
     * @param pIndex the player's index.
     * @param moves the possible moves.
     * @see https://elearning.esi.heb.be/courses/DEVG4A/document/cppl2/s1/quoridor.pdf page 4
     */
    void getPossibleMoves(int pIndex, vector<position>& moves) const;

    /**
     * @brief Moves player to a new position.
     * Move is possible if position is part of the possible moves of a player.
     * @param playerIndex the player index.
     * @param newPos the new position.
     * @return true if move succesful else false.
     * @see board::getPossibleMoves();
     */
    bool movePlayer(int playerIndex, position newPos);

    /**
     * @brief Checks if a player is in a certain position or not.
     * @param at the position that need to be checked.
     * @return true if there is and false if not.
     */
    bool isPlayerPresent(position at) const;

    void testPrint();//retirer ou pas ..
    void printDebug();

    /**
     * @brief Gets the index of the player in a given position.
     *
     * @return the index of a player.
     */
    int getPlayerAtPosition( const position & at) const;

    /**
     * @brief Checks if the position and orientation of a wall is valid.
     * @param playerIndex the player's index.
     * @param at the position of wall placement.
     * @param orientation th way wall is oriented
     * @return false when wall is out of board or when player has no wall left and when postion is center of wall otherwise returns true.
     * @see board::positionIsCenterOfWall()
     * @see player::hasWall()
     */
    bool canPlaceWall(int playerIndex, position at, direction  orientation) const;

    /**
     * @brief Place wall at given position.
     * @param playerIndex the player's index.
     * @param at the position of wall placement.
     * @param orientation th way wall is oriented
     * @return false if the board could not be placed otherwise returns true if all players have a path to victory.
     */
    bool placeWall(int playerIndex, position& at, direction& orientation);

    /**
     * @brief Gets the game's board.
     * @return the game's board .
     */
    inline const vector <vector<square>> & getBoard() const;

    /**
     * @brief Gets a player.
     * @param playerIndex the of the player.
     * @return a reference to the player.
     */
    inline const player& getPlayer(int playerIndex) const;

    /**
     * @brief Checks if there is a player at a winning position.
     * @return true if the board has a winner false if not.
     */
    bool boardHasWinner() const;
    /**
     * @brief Gets all players present in board.
     * @return the list of players present in board.
     */
    inline const vector <player *>& getAllPlayers() const;

    /**
     * @brief Returns whether a jum is in a player's set of moves.
     * @param player th player to check for
     * @return  whether given player can jump
     */
    inline bool canJump(int player) const;

};

int board::getSize() const {
    return size;
}

const vector <vector<square>>&  board::getBoard() const {
    return tiles;
}

const player& board::getPlayer(int playerIndex) const{
    return *players.at(playerIndex);
}

const vector <player*>& board::getAllPlayers() const{
    return players;
}

bool board::canJump(int player) const {
    vector<position> poss;
    getPossibleMoves(player, poss);
    square t = tiles[players[player]->getPosition().getY()][players[player]->getPosition().getX()];
    bool isJump = false;
    for (position pos: poss) {
        for (std::pair<direction, const square*> s: t.getNeighbors()) {
            isJump = !(s.second->getPosition() == pos); // Negating a == here... Cuz didn't override != hehe O:-)
            if (!isJump) break;
        }
        if (isJump) return true;
    }
    return false;
}
}//end of Quoridor
#endif // BOARD_H
