/**
 * @file position.h
 * @brief Definition of class Quoridor::position.
 */
#ifndef POSITION
#define POSITION

#include <stdexcept>

/**
 * @brief namespace for Quoridor
 */
namespace Quoridor {


/**
 * @brief A position class.
 * This class provides simple position méthods using XY coardinates that can be used to locate an element.
 */

class position
{
private:
    int x;//Int or unsigned ? int because of range check in board.
    int y;

public:

    inline position(); //required for std::vector<>

    /**
     * @brief Creates a position using XY coordinates.
     * Creates a position with specifique XY coordinates that can later be changed.
     * @param x the x coordinate of the position that's to be created.
     * @param y the y coordinate of the position that's to be created.
     * @throw range_error if x or y parameters are not positive nombers.
     * @see position::setXY() for more info on how to change the position.
     */
    inline position(int x,int y);

    /**
     * @brief Gets a copie of the X coordinates of the position.
     * Creates a position with a specifique XY coordinates that can later be changed.
     * @return the y coordinate of the position.
     */
    inline int getX() const;

    /**
     * @brief Gets a copie of the Y coordinates of the position.
     * Gets a copie of the Y coordinates of the position.
     * @return the y coordinate of the position.
     */
    inline int getY() const;

    /**
     * @brief Sets the position to a new XY coordinates.
     * Sets the position to a new XY coordinates.
     * x and y are unsigned to avoid rangecheck.
     * @param x the new x coordinate of the position.
     * @param y the new y coordinate of the position.
     */
    inline void setXY(unsigned x , unsigned y);
};
position::position():x(0),y(0) {}

position::position(int x, int y):x(x) ,y(y) {
    if (x < 0) throw std::range_error("x has to be positive! Received " + std::to_string(x));
    if (y < 0) throw std::range_error("y has to be positive! Received " + std::to_string(y));
}

int position::getX() const {return x;}
int position::getY() const {return y;}
void position::setXY(unsigned x, unsigned y) {this->x = x; this->y = y;}

inline bool operator== (const position p1, const position p2) {
    return p1.getX() == p2.getX() && p1.getY() == p2.getY();
}

}//end of Quoridor
#endif // POSITION

