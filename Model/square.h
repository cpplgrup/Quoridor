/**
 *@file square.h
 *@brief Definition of class Quoridor::square.
 */

#ifndef SQUARE
#define SQUARE

#include <map>
#include <stdexcept>
#include "position.h"
#include "player.h"
#include "direction.h"

/**
 * @brief namespace for Quoridor
 */
namespace Quoridor {

/**
 * @brief The square class, a board's tile.
 * This class represents a board's tile and is modeled on a graph nodes.
 * The only difference is that instead of containing a list of edges to nodes it containes direct links to other nodes.
 */
class square {
private:
    position POS;
    std::map<direction, square * const> neighbors;
    bool marked;

public:
    inline square();//has to be private and = Delete

    /**
     * @brief Creates a new square frome given position.
     * Creates a new square frome given position.
     * @param pos the position of the square.
     */
    inline  square(position& pos) ;//has to be private and = Delete

    /**
     * @brief Gets the position of the square.
     * Gets the position of the square.
     * @return the square's position.
     */
    inline const position &  getPosition() const;

    /**
     * @brief Adds a square to liste of neighbors on e given edge.
     * Adds a square to liste of neighbors only one neighbors per edge is possible.
     * @param dir the square's edge were the neighbor is from.
     */
    inline void addNeighbor(direction dir, square &);

    /**
     * @brief Removes neighbor from the list of neighbors.
     * @param dir the square's edge where neighbor is.
     */
    inline void removeNeighbor(direction);

    /**
     * @brief Gets the square's list of neighbors.
     * @return the liste of neighbors not that it can't be modified.
     */
    inline const std::map<direction, square * const> &getNeighbors() const;

    /**
     * @brief Check if square has a neighbor in the given direction.
     * @return true if yes fals if not
     */
    inline bool hasNeighbor(direction) const;

    /**
     * @brief Marks a square.
     *  Marks a square can be useful when traveling trough a graph of squares.
     */
    inline void mark();

    /**
     * @brief Unmarks a square.
     *  Unmarks a square can be useful when traveling trough a graph of squares.
     */
    inline void unMark();

    /**
     * @brief Checks if a square is marked or not.
     * @return true if marked false if not.
     */
    inline bool isMarked() const;
};

square::square() {}

square::square(position &pos): POS(pos) , marked(false) {}

void square::addNeighbor(direction dir, square & neighbor) {
    neighbors.insert(std::pair<direction, square* const> (dir, &neighbor));
}

const position & square::getPosition() const {
    return POS;
}

const std::map<direction, square * const> &square::getNeighbors() const {
    return neighbors;
}

void square::removeNeighbor(direction dir) {
    neighbors.erase(dir);
}

inline std::ostream& operator <<(std::ostream& os, square& sq) {  // Required by catch
    os << "Square ("<<sq.getPosition().getX()<<", "<<sq.getPosition().getY()<<") Neighbors : ";
    for (std::pair<direction, const square*> nei : sq.getNeighbors())
        os << nei.first << "(" << nei.second->getPosition().getX() << ", " <<nei.second->getPosition().getY() << ")  ";

    return os;
}

bool square::hasNeighbor(direction d) const {
    return neighbors.find(d)!= neighbors.end();
}

void square::mark() {
    marked =  true;
}

bool square::isMarked() const {
    return marked;
}

void square::unMark(){
    marked =  false;
}
}//end of Quoridor
#endif // SQUARE

