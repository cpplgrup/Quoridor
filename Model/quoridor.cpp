#include "quoridor.h"

using namespace Quoridor;

quoridor::quoridor(unsigned numPlayers, int size, std::vector<std::string> names):
    gameState(GameState::in_progress) , _board(numPlayers,size,names) , currentPlayer(0),numPlayers(numPlayers) {
    if (size % 2 != 1) throw std::range_error("Size has to be uneven! Got size: " + std::to_string(size));
    if (numPlayers != 2 && numPlayers != 4) throw std::range_error("Players have to be 2 or 4! Received: " + std::to_string(numPlayers));
    if (size < 5 || size > 19) throw std::range_error("Size has to be between 5 and 19! Got size: " + std::to_string(size));

//    if (names.size() == numPlayers) for (unsigned i = 0; i < players.size(); i++) players[i] = player(names[i], size, getDir(i), numPlayers);
//    else throw std::range_error("Didn't provide right names count! Provided: " + std::to_string(names.size()) + ", required: " + std::to_string(numPlayers));

}

