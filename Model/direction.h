/**
 * @file direction.h
 * @brief Definition of enum class Quoridor::direction.
 */
#ifndef DIRECTION
#define DIRECTION

#include <ostream>
#include <iostream>
#include <stdexcept>
#include <vector>
#include "position.h"

/**
 *@brief namespace for Quoridor
 */
namespace Quoridor {

/**
 * @brief The direction enum class.
 * The direction enum class is a strongly typed enum use for given geographical map coordinates.
 */
enum class direction {
    north,
    east,
    south,
    west,
    nodir // Because can't make pointers to these >.<
};


/**
 * @brief operator << used for debuging..
 * @param os the output stream
 * @param coord the direction to print
 * @return the output stream with the print.
 */
inline std::ostream& operator <<(std::ostream& os, direction& coord) {  // For CATCH
    switch(coord) {
        case direction::north: os << "N"; break;
        case direction::east: os << "E"; break;
        case direction::south: os << "S"; break;
        case direction::west: os << "W"; break;
        default: os << "?";
    }
    return os;
}

/**
 * @brief Gets a direction from an integer.
 * @param a the integer of the wanted direction.
 * @return the direction asked for.
 * @throw range_error if the int is not between 0 and 3
 */
inline direction getDir(int a) {
    switch (a) {
        case 0: return direction::north; break;
        case 1: return direction::south;  break;
        case 2: return direction::west; break;
        case 3: return direction::east; break;
    }
    throw std::range_error(std::to_string(a) + "out of range (0 - 3)");
}

/**
 * @brief Gets the starting position.
 * Gets the starting position from a given origin a board's size.
 * @param dir the direction.
 * @param size the board's size.
 * @return the starting position.
 */
inline position getStartingPos(direction& dir, int size) {
    switch(dir) {
        case direction::north: return position(size / 2, 0); break;
        case direction::east: return position(size - 1, size / 2); break;
        case direction::south: return position(size/2, size - 1); break;
        case direction::west: return position(0, size / 2); break;
        default: throw std::range_error("direction::getStartingPos() received incorrect direction. Should never happen.");
    }
}

/**
 * @brief Get's the opposite direction.
 * @param dir th given direction.
 * @return de direction if
 */
inline direction getOppositeDir(direction& dir) {
    switch(dir) {
        case direction::north: return direction::south; break;
        case direction::east: return direction::west; break;
        case direction::south: return direction::north; break;
        case direction::west: return direction::east; break;
        default: throw std::range_error("direction::getStartingPos() received incorrect direction. Should never happen.");
    }
}

/**
 * @brief Checks if a direction is vertical.
 * @param orientation direction  to be check.
 * @return true if vertical else false.
 */
inline bool isVertical(direction& orientation) {
    return orientation == direction::north || orientation == direction::south;
}

/**
 * @brief Checks if a position is winning position.
 * @param pos the actual position.
 * @param origin the diection of origin.
 * @param size the size of the board.
 * @return true if is winnig position, false if not
 */
inline bool isWinningPosition(const position & pos, direction& origin, int size) {
    switch (origin) {
        case direction::north: return  pos.getY() == (size- 1); break;
        case direction::east:  return  pos.getX() == 0; break;
        case direction::south: return  pos.getY() == 0; break;
        case direction::west:  return  pos.getX() == (size- 1); break;
        default: return false;
    }
}



}//Namespace QUORIDOR
#endif // DIRECTION

