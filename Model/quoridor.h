/**
 *@file quoridor.h
 *@brief Definition of class Quoridor::quoridor.
 */
#ifndef QUORIDOR
#define QUORIDOR

#include "board.h"
#include "gamestate.h"

/**
 * @brief namespace for Quoridor
 */
namespace Quoridor {

/**
 * @brief Class defines basic rules needed for to play quoridor.
 * This class provides simple rules about the gameplay of quoridor.
 */
class quoridor
{
private:
    GameState gameState;
    board _board;
    int currentPlayer;
    int numPlayers;

protected:
    /**
     * @brief Changes the value of the current player.
     * Changes the value of the current player from 0 to numPlayers - 1 and then repeats until the game is ended.
     */
    virtual inline void changePlayer();

public:
    /**
     * @brief Creates a new quoridor game.
     * Creates a new quoridor game by making the game board with all the players on it,
     * and setting the currentPlayer and gameState to there default value(O and GameState::in_progress).
     * @param numPlayers the number of that are playing the game.
     * @param size the size of the game board.
     * @param names the list of player names that are playing the game.
     * @throw range_error if size is not uneven or not between 5 and 19.
     * @throw range_error if numPlayerse is not equal to 2 or 4 and not equal to the number of names given.
     * @see board
     */
    quoridor(unsigned numPlayers, int size, std::vector<std::string> names);

    /**
     * @brief Checks if the game is a draw.
     * The game is considered a draw when the current player can't move or doesn't have walls left.
     * This method sets the gameState to GameState::draw  if it's a draw.
     * @return true if draw and false if not.
     */
    inline bool checkDraw();

    /**
     * @brief Cheks if the game has a winner.
     * The game is considered won when the current player is in winning position.
     * This method sets the gameState to GameState::won  if it hase a winner.
     * @return true if has winner, false if not.
     * @see board::boardHasWinner() for more info.
     */
    inline bool checkWinner();

    /**
     * @brief Gets refrence to the actual game state.
     * Gets refrence to the actual game state. This gameState is unmodifiable
     * @return the game's state.
     */
    inline const GameState& getGameState() const;

    /**
     * @brief setGameState
     */
    inline void setGameState(GameState);

    /**
     * @brief Gets the actual game board.
     * @return the  game board.
     */
    inline const board& getBoard() const;

    /**
     * @brief Gets the index of the current player.
     * @return current player index.
     */
    inline int getCurrentPlayer() const;

    /**
     * @brief getNumPlayers
     * @return
     */
    inline int getNumPlayers() const; // Je peux le retirer il est pas utilisé

    /**
     * @brief Attempts to move the current player to a new position geving a position.
     * Attempts to move the current player to a new position geving a position.
     * @param newPos the position where the player wants to be moved to
     * @return true if move successful false if not.
     */
    virtual inline bool movePlayer(position& newPos);

    /**
     * @brief Attempts to move the current player to a new position giving directions.
     * Computes a new position from the given direction.
     * Then eventually calls quoridor::movePlayer(position&) to attempt to move the current player to the new position.
     * @param dir the direction where the player wants to be moved to
     * @return true if move successful false if not.
     */
    virtual inline bool movePlayer(direction& dir);

    /**
     * @brief Attempts to place a wall in the position given by the current player.
     * Attempts to place a wall in the position given by the current player. The
     *
     * @param pos the position of the center of the the wall.
     * @param orientation the orientation vertical(north,south) or horizontal (east,west).
     * @return true if wall was placed false if not.
     */
    virtual inline bool placeWall(position& pos, direction orientation);

};

void quoridor::changePlayer(){
    currentPlayer = (currentPlayer + 1) % numPlayers;
}
const GameState & quoridor::getGameState() const{
    return gameState;
}

void quoridor::setGameState(GameState s) {
    gameState = s;
}

const board & quoridor::getBoard() const {
    return _board;
}

int quoridor::getCurrentPlayer() const {
    return currentPlayer;
}

int quoridor::getNumPlayers() const {
    return numPlayers;
}

bool quoridor::checkWinner() {
    if(_board.boardHasWinner()){
        gameState = GameState::won;
        return true;
    }
    return false;
}

bool quoridor::checkDraw() {
    vector<position> allMoves;
    _board.getPossibleMoves(currentPlayer,allMoves);
    if(allMoves.size() == 0 && _board.getPlayer(currentPlayer).getNbWall() == 0){
        gameState = GameState::draw;
        return true;
    }
    return false;
}

bool quoridor::movePlayer(position &newPos) {
    if(! _board.movePlayer(currentPlayer , newPos)) return false;
    checkDraw();
    checkWinner();
    return true;
}

bool quoridor::movePlayer(direction& dir) {
    int x = _board.getPlayer(currentPlayer).getPosition().getX();
    int y = _board.getPlayer(currentPlayer).getPosition().getY();
    switch(dir) {
    case direction::north: y -= 1; break;
    case direction::south: y += 1; break;
    case direction::west: x -= 1; break;
    case direction::east: x += 1; break;
    default: break;
    }

    if (x < 0 || y < 0) return false;

    position newPos(x, y);

    return movePlayer(newPos);
}

bool quoridor::placeWall(position & pos, direction orientation) {
    if( !_board.placeWall(currentPlayer, pos , orientation)) return false;
    return true;
}

}//end of Quoridor
#endif // QUORIDOR

