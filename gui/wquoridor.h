#ifndef WQUORIDOR_H
#define WQUORIDOR_H

#include <QWidget>
#include <observablequoridor.h>
#include <qgridlayout.h>
#include "quoridorobserver.h"
#include "tile.h"

using namespace Quoridor;

class wQuoridor : public QWidget, public QuoridorObserver
{
    Q_OBJECT
public:
    explicit wQuoridor(ObservableQuoridor *q, QWidget *parent = 0);
    ~wQuoridor();

    /**
     * @brief Prints out game in desired format.
     * @param event change event.
     */
    void printGame(ChangeEvent event);

    /**
     * @brief Prints out the game and a line to notify players that a player have moved
     * @param event the change event.
     */
    void notifyMovePlayer(ChangeEvent event);

    /**
     * @brief Prints out the game and a line to notify players that a wall as been placed
     * @param event the change event.
     */
    void notifyPlaceWall(ChangeEvent event);

    void notifyChangePlayer(ChangeEvent event);

    void mousePressEvent(QMouseEvent *ev);

private:
    ObservableQuoridor *_quoridor;
    QGridLayout *grid = nullptr;
    void refreshScreen();
    void resetTiles();

protected:
    void paintEvent(QPaintEvent *event);
signals:

private slots:
    void Mouse_Pressed(Tile *t);
};

#endif // WQUORIDOR_H
