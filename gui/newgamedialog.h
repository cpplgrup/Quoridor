#ifndef NEWGAMEDIALOG_H
#define NEWGAMEDIALOG_H

#include <QDialog>
#include <vector>

using namespace std;
namespace Ui {
class NewGameDialog;
}

class NewGameDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewGameDialog(QWidget *parent = 0);
    ~NewGameDialog();
    int getNumOlayers();
    map<string,bool> getNames();
    int getSize();

private:
    Ui::NewGameDialog *ui;
    void connexion();
private slots:
    void changePlayer();
};

#endif // NEWGAMEDIALOG_H
