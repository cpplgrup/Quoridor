include(../defaults.pri)

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gui
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    newgamedialog.cpp \
    wquoridor.cpp \
    tile.cpp

HEADERS  += mainwindow.h \
    newgamedialog.h \
    wquoridor.h \
    tile.h

FORMS    += mainwindow.ui \
    newgamedialog.ui

LIBS += -L../lib -llibView \
    -L../lib -llibModel

CONFIG += console

CONFIG += c++11

RESOURCES += \
    img/resource.qrc
