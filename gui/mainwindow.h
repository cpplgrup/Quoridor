#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAction>
#include "newgamedialog.h"
#include "observablequoridor.h"
#include "wquoridor.h"

using namespace Quoridor;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    wQuoridor *wquoridor;
    void connexion();
public slots:
    void createGame();
    void endGame();
};


#endif // MAINWINDOW_H
