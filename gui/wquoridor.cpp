#include "mainwindow.h"
#include "wquoridor.h"

#include <QMessageBox>
#include <qpushbutton.h>

wQuoridor::wQuoridor(ObservableQuoridor *quoridor, QWidget *parent) : QWidget(parent), _quoridor(quoridor)
{
    _quoridor->registerObserver(this);
    this->setGeometry(0, 0, 600, 600);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    grid = new QGridLayout(this);
    for (auto line: _quoridor->getBoard().getBoard()) for (auto square: line) {
        position p = square.getPosition();
        Tile *t = new Tile(square);
        connect(t, SIGNAL(Mouse_Pressed(Tile *)), this, SLOT(Mouse_Pressed(Tile *)));
        grid->addWidget(t, p.getY(), p.getX()); // Changed x and y (EDIT)
    }
    setLayout(grid);
    refreshScreen();
}

wQuoridor::~wQuoridor()
{
    delete _quoridor;
    delete grid;
    // Tiles should be deleted by the parent...
}

void wQuoridor::printGame(ChangeEvent event){
    refreshScreen();
}

void wQuoridor::notifyChangePlayer(ChangeEvent event){
    refreshScreen();
}

void wQuoridor::refreshScreen() {
    if (grid == nullptr) return;
    resetTiles();
    int i = 0;
    for (player* a: _quoridor->getBoard().getAllPlayers()) {
        position p = a->getPosition();
        QLayoutItem *b = grid->itemAtPosition(p.getY(), p.getX());
        Tile *t = dynamic_cast<Tile*>(b->widget());
        if (i == _quoridor->getCurrentPlayer()) {
            vector<position> v;
            _quoridor->getBoard().getPossibleMoves(i, v);
            t->setCurrentPlayer(true);
            for (position p1: v) {
                QLayoutItem *b1 = grid->itemAtPosition(p1.getY(), p1.getX());
                Tile *t1 = dynamic_cast<Tile*>(b1->widget());
                t1->setHighlight(true);
            }
        }
        t->setPlayer(i++);
    }
    update();
    if (_quoridor->getGameState() != GameState::in_progress) {
        QString title("Game Over!");
        QString content;
        switch (_quoridor->getGameState()) {
        case GameState::draw: content = ("The game ended in a draw.");break;
        case GameState::won: content = ("The game was won by ");content += _quoridor->getBoard().getPlayer(_quoridor->getCurrentPlayer()).getName().c_str();break;
        case GameState::quit: content = ("Someone has quit. Shouldn't happen??!");
        }

        QMessageBox::information(this, title, content); // TODO: why does this shit appear twice??!

        for (auto a: children()) {
            if (a != grid) {
                Tile *t = dynamic_cast<Tile*>(a);
                disconnect(t, SIGNAL(Mouse_Pressed(Tile *)), this, SLOT(Mouse_Pressed(Tile *)));
            }
        }
    }
}

void wQuoridor::resetTiles()
{
    for (auto a: children()) {
        if (a != grid) {
            Tile *t = dynamic_cast<Tile*>(a);
            t->setHighlight(false);
            t->setCurrentPlayer(false);
            t->setPlayer(-1);

            // EDITED
            position p = t->getSquare().getPosition();
            t->updateTo(_quoridor->getBoard().getBoard().at(p.getY()).at(p.getX()));
            // END OF EDIT
        }
    }
}

void wQuoridor::paintEvent(QPaintEvent *event)
{

}

void wQuoridor::Mouse_Pressed(Tile *t)
{
    int x, y, unused;
    grid->getItemPosition(grid->indexOf(t), &x, &y, &unused, &unused);
    _quoridor->movePlayer(position(y, x));
}

void wQuoridor::mousePressEvent(QMouseEvent *ev)
{
    int spacing = grid->spacing();
    int xpos = ev->x() - spacing * 1.5;
    int ypos = ev->y() - spacing * 1.5;
    QRect r = grid->cellRect(0, 0);
    int x = (xpos + r.width() / 2) / (spacing + r.width()) - 1;
    int y = (ypos + r.height() / 2) / (spacing + r.height()) - 1;

    bool v = (xpos + spacing) % (r.width() + spacing) <= spacing;
    bool h = (ypos + spacing) % (r.height() + spacing) <= spacing;
    if (v != h) _quoridor->placeWall(position(x, y), v? direction::north: direction::east);
}

void wQuoridor::notifyMovePlayer(ChangeEvent event){
    refreshScreen();
}

void wQuoridor::notifyPlaceWall(ChangeEvent event){
    refreshScreen();
}


