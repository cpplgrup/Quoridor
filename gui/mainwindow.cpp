#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connexion();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::createGame(){
    NewGameDialog ng {this};
    auto data = ng.exec();
    if(data == QDialog::Rejected) return;
    map<string,bool> names = ng.getNames();
    int numPlayers = ng.getNumOlayers();
    int size = ng.getSize();
    try {
        wquoridor = new wQuoridor(new ObservableQuoridor(numPlayers, size, names), ui->centralWidget);
    } catch (const std::exception & exception){
        QMessageBox::warning(this, tr("Erreur configuration"), QString(exception.what()));
        return;
    }
    ui->actionNew->setEnabled(false);
    ui->actionEnd->setEnabled(true);
    delete ui->wQuoridor;
    ui->wQuoridor = wquoridor;
    wquoridor->setEnabled(true);
    wquoridor->show();
}

void MainWindow::endGame()
{
    delete wquoridor;
    ui->actionNew->setEnabled(true);
    ui->actionEnd->setEnabled(false);
    wquoridor->hide();
}

void MainWindow::connexion(){
    connect(ui->actionNew, &QAction::triggered, this, &MainWindow::createGame);
    connect(ui->actionEnd, &QAction::triggered, this, &MainWindow::endGame);
    connect(ui->actionQuit, &QAction::triggered, &QCoreApplication::quit);
}
