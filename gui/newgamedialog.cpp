#include "newgamedialog.h"
#include "ui_newgamedialog.h"

NewGameDialog::NewGameDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewGameDialog)
{
    ui->setupUi(this);
    connexion();
}

void NewGameDialog::connexion(){
     connect(ui->rbTwo, &QRadioButton::toggled, this,&NewGameDialog::changePlayer);
     connect(ui->rbFour,&QRadioButton::toggled, this,&NewGameDialog::changePlayer);
}

NewGameDialog::~NewGameDialog()
{
    delete ui;
}

int NewGameDialog::getNumOlayers() {
    if(ui->rbTwo->isChecked()) {
        return 2;
    }
    return 4;
}

map<string,bool> NewGameDialog::getNames() {
    map<string,bool> names;

      names.insert(pair<string, bool>(ui->tfPlayer_1->text().toStdString(),ui->ai_1->isEnabled()));
      names.insert(pair<string, bool>(ui->tfPlayer_2->text().toStdString(),ui->ai_2->isEnabled()));
     if (ui->rbFour>isChecked()) {
         names.insert(pair<string, bool>(ui->tfPlayer_3->text().toStdString(),ui->ai_3->isEnabled()));
         names.insert(pair<string, bool>(ui->tfPlayer_4->text().toStdString(),ui->ai_4->isEnabled()));
    }
    return names;
}

int NewGameDialog::getSize() {
    return ui->sbBoardSize->value();
}

void NewGameDialog::changePlayer(){
    if(ui->rbTwo->isChecked()){
        ui->tfPlayer_3->setEnabled(false);
        ui->tfPlayer_4->setEnabled(false);
        ui->ai_3->setEnabled(false);
        ui->ai_4->setEnabled(false);
    }else{
        ui->tfPlayer_3->setEnabled(true);
        ui->tfPlayer_4->setEnabled(true);
        ui->ai_3->setEnabled(true);
        ui->ai_4->setEnabled(true);
    }
}


