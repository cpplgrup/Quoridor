#include "tile.h"

#include <QPainter>
#include <direction.h>

Tile::Tile(Quoridor::square sq): square(sq)
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
}

Tile::~Tile()
{

}

void Tile::mousePressEvent(QMouseEvent *ev)
{
    emit Tile::Mouse_Pressed(this);
}

// EDITED
void Tile::updateTo(Quoridor::square to)
{
    square = to;
}

Quoridor::square Tile::getSquare()
{
    return square;
}
// END OF EDIT

void Tile::setPlayer(int player)
{
    playerId = player;
}

void Tile::setHighlight(bool on)
{
    highlight = on;
}

void Tile::setCurrentPlayer(bool on)
{
    currentPlayer = on;
}

void Tile::paintEvent(QPaintEvent *event)
{
    QRect rect = event->rect();
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setPen(Qt::black);
    if (currentPlayer) {
        QColor c(Qt::cyan);
        c.setAlpha(128);
        painter.fillRect(rect, c);
    }
    painter.drawRect(rect);
    if (highlight) painter.fillRect(rect, Qt::cyan);
    painter.setRenderHint(QPainter::SmoothPixmapTransform);
    switch (playerId) {
    case 0: painter.drawPixmap(rect, QPixmap(":img/players/blue.png"));break;
    case 1: painter.drawPixmap(rect, QPixmap(":img/players/green.png"));break;
    case 2: painter.drawPixmap(rect, QPixmap(":img/players/red.png"));break;
    case 3: painter.drawPixmap(rect, QPixmap(":img/players/yellow.png"));break;
    }
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setPen(Qt::red);
    if (!square.hasNeighbor(Quoridor::direction::north)) painter.drawLine(rect.topLeft(), rect.topRight());
    if (!square.hasNeighbor(Quoridor::direction::east)) painter.drawLine(rect.topRight(), rect.bottomRight());
    if (!square.hasNeighbor(Quoridor::direction::south)) painter.drawLine(rect.bottomLeft(), rect.bottomRight());
    if (!square.hasNeighbor(Quoridor::direction::west)) painter.drawLine(rect.topLeft(), rect.bottomLeft());
}
