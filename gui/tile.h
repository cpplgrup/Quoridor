#ifndef TILES_H
#define TILES_H

#include <QPaintEvent>
#include <QRect>
#include <qwidget.h>
#include <iostream>
#include <square.h>

class Tile : public QWidget
{
    Q_OBJECT
public:
    Tile(Quoridor::square sq);
    ~Tile();
    void setPlayer(int player);
    void setHighlight(bool on);
    void setCurrentPlayer(bool on);
    void mousePressEvent(QMouseEvent *);

    // EDITED
    void updateTo(Quoridor::square);
    Quoridor::square getSquare();
    // END OF EDIT
signals:
    void Mouse_Pressed(Tile *);
private:
    int playerId = -1;
    bool highlight = false;
    bool currentPlayer = false;
    Quoridor::square square;
protected:
    void paintEvent(QPaintEvent *event);
};

#endif // TILES_H
