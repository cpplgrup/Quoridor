include(../defaults.pri)

TEMPLATE = app

LIBS +=\
    -L../lib -llibModel



CONFIG += console

HEADERS += \
    catch.hpp

SOURCES += \
    main.cpp \
    test_player.cpp \
    test_square.cpp \
    test_board.cpp

CONFIG -= app_bundle
CONFIG -= qt
