#include "catch.hpp"
#include "player.h"
#include "dude.h"
using namespace Quoridor;

SCENARIO("Let's see what happens when we create a player","[player] [constructor]"){

    GIVEN("Origin north with a board size of 9"){
        Dude p("player",9,direction::north,2);
        THEN("Player should be in position (4,2)"){
            REQUIRE(p.getPosition() == position(4,0));
        }
    }

    GIVEN("Origin east with a board size of 9"){
        Dude p("player",9,direction::east,2);
        THEN("Player should be in position (4,4)"){
            REQUIRE(p.getPosition() == position(8,4));
        }
    }

    GIVEN("Origin south with a board size of 9"){
        Dude p("player",9,direction::south,2);
        THEN("Player should be in position (0,0)"){
            REQUIRE(p.getPosition() == position(4,8));
        }
    }

    GIVEN("Origin west with a board size of 9"){
        Dude p("player",9,direction::west,2);
        THEN("Player should be in position (0,0)"){
            REQUIRE(p.getPosition() == position(0,4));
        }
    }

    GIVEN("number of player of 2 and a board size of 9"){
        Dude p("player",9,direction::south,2);
        THEN("Player should have 10 walls"){
            REQUIRE(p.getNbWall() == 10);
        }
    }

    GIVEN("number of player of 4 and a board size of 9"){
        Dude p("player",9,direction::south,4);
        THEN("Player should have 10 walls"){
            REQUIRE(p.getNbWall() == 5);
        }
    }
}



SCENARIO("Let's see if a player can be moved","[player]"){

    GIVEN("A new player in position (4,0)"){
        Dude p("player",9,direction::north,2);
        WHEN("we move player to position (4,2)"){
            p.move(position(4,2));
            THEN("Player should be in position (4,2)"){
                REQUIRE(p.getPosition() == position(4,2));
            }
        }
    }
}

SCENARIO("Getting the position of a player","[player] [getters]"){

    GIVEN("A new player in position (4,0)"){
        Dude p("player",9,direction::north,2);
        WHEN("we get the postion of the player and place into variable pos"){
            position pos = p.getPosition();
            THEN("player and pos should be in the same position"){
                REQUIRE(p.getPosition() == pos);
            }
            AND_WHEN("we change value of pos"){
                pos.setXY(4,2);
                THEN("player and pos position should defer"){
                    REQUIRE_FALSE(p.getPosition() == pos);
                }
            }
        }
    }
}

SCENARIO("Getting the Origin of a player","[player] [getters]"){

    GIVEN("A new player from the north"){
        Dude p("player",9,direction::north,2);
        WHEN("we get the origin of the player and place into variable origin"){
            direction origin = p.getOrigin();
            THEN("player's origin and origin should be north"){
                REQUIRE(p.getOrigin() == origin);
            }
            AND_WHEN("we change value of origin"){
                origin = direction::south;
                THEN("player and origin value should defer"){
                    REQUIRE_FALSE(p.getOrigin() == origin);
                }
            }
        }
    }
}
