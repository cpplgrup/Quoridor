#include "catch.hpp"
#include "square.h"

using namespace Quoridor;

SCENARIO("Square has neighbors ","[square]"){

    GIVEN("a new square"){
        position pos(4,2);
        square sq(pos);
        THEN("on call for hasWall it should return false no matter the direction"){
            REQUIRE_FALSE(sq.hasNeighbor(direction::north));
            REQUIRE_FALSE(sq.hasNeighbor(direction::east));
            REQUIRE_FALSE(sq.hasNeighbor(direction::south));
            REQUIRE_FALSE(sq.hasNeighbor(direction::west));
        }

        WHEN("we add a neighbor in the north"){
            pos = position(5,3);
            square sq2(pos);
            sq.addNeighbor(direction::north,sq2);
            THEN(" the square should have a neigbhor in the north"){
                REQUIRE(sq.hasNeighbor(direction::north));
            }
            AND_WHEN("we remove this neighbor"){
                sq.removeNeighbor(direction::north);
                THEN("sq should have nobody as neigbhor"){
                    REQUIRE_FALSE(sq.hasNeighbor(direction::north));
                }
            }
        }

    }
}

SCENARIO("Getting square's neighbors","[square] [getters]"){

    GIVEN("a new square"){
        position pos(4,2);
        square sq(pos);
        WHEN("get his neighbors"){
            THEN("Then the return container should be empty"){
                REQUIRE(sq.getNeighbors().size() == 0);
            }
        }
        AND_WHEN("We had some neighbors"){
            pos = position(3,3);
            square sq2(pos);
            pos = position(4,3);
            square sq3(pos);
            square sq4(pos);
            sq.addNeighbor(direction::north,sq2);
            sq.addNeighbor(direction::north,sq3);
            sq.addNeighbor(direction::north,sq4);
            THEN("Then the return container should not be empty"){
                REQUIRE(sq.getNeighbors().size() != 0);
            }
            AND_THEN("if we try to change the containt of the returned container square should not be affected"){
                std::map<direction, square * const> neighbors  = sq.getNeighbors();
                neighbors.empty();
                REQUIRE(sq.getNeighbors().size() != 0);
            }
        }
    }
}

