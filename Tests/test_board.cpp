#include "catch.hpp"
#include "board.h"
#include <algorithm>

using namespace Quoridor;

SCENARIO("What happens when we get the possible moves of a player","[board]"){

    vector<string> names{"le1","le2","le3","le4"};
    vector<position> moves;
    board b(4,5,names);
    GIVEN("a new board of size 5 with 4 playes in it."){
        THEN("if we get the posible moves of all player it should be 3 possibilities"){
            b.getPossibleMoves(0,moves);
            REQUIRE(moves.size() == 3);
            moves.clear();
            b.getPossibleMoves(1,moves);
            REQUIRE(moves.size() == 3);
            moves.clear();
            b.getPossibleMoves(2,moves);
            REQUIRE(moves.size() == 3);
            moves.clear();
            b.getPossibleMoves(3,moves);
            REQUIRE(moves.size() == 3);

            AND_WHEN("we look at the position recieve for player 4(east)"){
                THEN("We se that the position ar correct"){
                    REQUIRE((find(moves.begin(),moves.end(),position(4,1))) != moves.end());
                    REQUIRE((find(moves.begin(),moves.end(),position(3,2))) != moves.end());
                    REQUIRE((find(moves.begin(),moves.end(),position(4,3))) != moves.end());
                } moves.clear();
                AND_WHEN("a wall is blocking a path"){
                    position pos(3,2);
                    direction dir(direction::north);
                    b.placeWall(0,pos,dir);
                    b.getPossibleMoves(3,moves);
                    THEN("Then it should be impossible for player to move to position 3,2 and move should be of size 2"){
                        REQUIRE(moves.size() == 2);
                        REQUIRE_FALSE((find(moves.begin(),moves.end(),position(3,2))) != moves.end());

                    }
                    moves.clear();
                }
            }
        }
        WHEN("We moves the players and players encounter each other"){
            b.movePlayer(0,position(2,1));
            b.movePlayer(0,position(2,2));
            b.movePlayer(1,position(2,3));
            THEN("if player his present in on of the original possible moves he will be able to jump over the other player if no wall is behind"){
                b.getPossibleMoves(0,moves);
                REQUIRE((find(moves.begin(),moves.end(),position(2,4))) != moves.end());
                moves.clear();
                AND_THEN("If there is a wall behind player the diagonal jump"){
                    position pos(2,3);
                    direction dir(direction::east);
                    b.placeWall(3,pos,dir);
                    b.getPossibleMoves(0,moves);
                    REQUIRE((find(moves.begin(),moves.end(),position(3,3))) != moves.end());
                    REQUIRE((find(moves.begin(),moves.end(),position(1,3))) != moves.end());
                    moves.clear();
                }
            }

        }
        AND_WHEN("It's imposible to move"){
            position pos(2,3);
            direction dir(direction::south);
            b.placeWall(3,pos,dir);
            pos =  position(1,3);
            dir = direction(direction::north);
            b.placeWall(3,pos,dir);
            pos =  position(2,2);
            dir = direction(direction::north);
            b.placeWall(3,pos,dir);

            b.movePlayer(0,position(2,1));
            b.movePlayer(0,position(2,2));
            b.movePlayer(0,position(2,3));
            b.movePlayer(2,position(1,2));
            b.movePlayer(2,position(2,2));

            THEN("The number of possible move should be 0"){
                b.getPossibleMoves(1,moves);
                REQUIRE(moves.size() == 0);//FIXE Me
                moves.clear();
            }
        }

    }
}


SCENARIO("Can a wall be placed ","[board]") {
    vector<string> names{"le1","le2","le3","le4"};
    board c(4,5,names);

    WHEN("when it's in the right postion and doesn't cut a wall and player has a wall "){
        THEN("It can be placed"){
            REQUIRE(c.canPlaceWall(1,position(1,3),direction::east ));
        }
    }
    WHEN("when player player dosen't have wall anymore"){
        position pos(2,3);
        direction dir(direction::south);
        c.placeWall(1,pos,dir);
        pos =  position(1,3);
        dir = direction(direction::north);
        c.placeWall(1,pos,dir);
        pos =  position(2,2);
        dir = direction(direction::north);
        c.placeWall(1,pos,dir);
        dir = direction(direction::north);
        c.placeWall(1,pos,dir);
        pos =  position(2,2);
        dir = direction(direction::north);
        c.placeWall(1,pos,dir);
        pos =  position(1,2);
        dir = direction(direction::north);
        c.placeWall(1,pos,dir);
        THEN("wall can't be placed"){
            REQUIRE_FALSE(c.canPlaceWall(1,position(1,3),direction::east ));
        }
    }
    WHEN("When you give position on the right and bottom edge"){
        THEN("wall can't be placed"){
            REQUIRE_FALSE(c.canPlaceWall(1,position(5,0),direction::east ));
        }
        THEN("wall can't be placed"){
            REQUIRE_FALSE(c.canPlaceWall(1,position(0,5),direction::east));
        }
    }
    WHEN("when give a position that is center of another wall") {
        position pos(2,3);
        direction dir(direction::south);
        c.placeWall(1,pos,dir);
        THEN("wall can't be placed"){
            REQUIRE_FALSE(c.canPlaceWall(1,position(2,3),direction::west ));
        }
    }

    WHEN("when give a position that is center of 2 other walls") {
        position pos(0,0);
        direction dir(direction::east);
        c.placeWall(1,pos,dir);
        pos =  position(2,0);
        dir = direction(direction::west);
        c.placeWall(1,pos,dir);
        pos =  position(2,0);
        dir = direction(direction::north);
        c.placeWall(1,pos,dir);
        THEN("wall can be placed"){
            REQUIRE(c.canPlaceWall(1,position(2,3),direction::west));
        }
    }

}


SCENARIO("Let's whant what happens wehen we place wall","[board]"){
    vector<string> names{"le1","le2","le3","le4"};
    board b(4,5,names);
    WHEN("Wall does not block a players path and can be placed"){
        position pos(2,3);
        direction dir(direction::south);
        THEN("placement will occur"){
            REQUIRE(b.placeWall(3,pos,dir));
        }
    }
    WHEN("wall can't be placed"){
        position pos(2,3);
        direction dir(direction::south);
        b.placeWall(3,pos,dir);
        dir =  direction(direction::west);
        THEN("placement will occur"){
            REQUIRE_FALSE(b.placeWall(3,pos,dir));
        }
    }
    WHEN("wall can't be placed"){
        position pos(1,0);
        direction dir(direction::south);
        b.placeWall(3,pos,dir);
        pos =  position(2,0);
        dir = direction(direction::north);
        b.placeWall(1,pos,dir);
        pos =  position(1,1);
        dir =  direction(direction::west);
        THEN("placement throw error message"){
            REQUIRE_THROWS(b.placeWall(3,pos,dir));
        }
    }
}

SCENARIO("Board's interaction with players"){
    vector<string> names{"le1","le2","le3","le4"};
    board b(4,5,names);
    WHEN("we try to see if player is in a certain square"){

        AND_WHEN("He's not in it"){
            THEN("It should return flase"){
                REQUIRE_FALSE(b.isPlayerPresent(position(1,1)));
            }
        }

        AND_WHEN("he's in it"){
            THEN("It should return true"){
                REQUIRE(b.isPlayerPresent(position(2,0)));
            }
        }
    }

    WHEN("we try an move a player to  new position in board"){

        AND_WHEN("the players can't move to the given position"){
            THEN("It should return flase")
                REQUIRE_FALSE(b.movePlayer(0,position(1,3)));
        }

        AND_WHEN("the players can move to the given position"){
            THEN("it returns true")
             REQUIRE(b.movePlayer(0,position(2,1)));
        }
    }

}
