@echo off
git add -A *
git reset -- Quoridor.pro.user
git reset -- Quoridor.7z
if not [%*] == [] (
    git commit -am "%*" --author "Arnaud Sol� <40931@heb.be>"
) else (
    git commit -a --author "Arnaud Sol� <40931@heb.be>"
)
pause