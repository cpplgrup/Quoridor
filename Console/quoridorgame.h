/**
 * @file quoridorgame.h
 * @brief Definition of class Quoridor::QuoridorGame.
 */
#ifndef QUORIDORGAME
#define QUORIDORGAME

#include <iostream>
#include "gamestate.h"
#include "observablequoridor.h"
#include "gameprinter.h"
#include <memory>

/**
 *@brief namespace for Quoridor
 */
namespace Quoridor {

/**
 * @brief The QuoridorGame class
 * This class generates an unique instance of quoridor game and interacts with players.
 */
class QuoridorGame
{
private:
    ObservableQuoridor game;
    shared_ptr<GamePrinter> output;

    /**
     * @brief Prompt user to make a choice
     * Prompt user to make a choice. They eather move or place a wall.
     */
    void makeChoice();
    /**
     * @brief Prints out the right end game sequence.
     * @param prevPlayer the previous player to have made a move.
     */
    void endGame(int prevPlayer);

    /**
     * @brief Prompt player for move.
     * @param text the input variable
     */
    void move(string& text);

    /**
     * @brief Prompt players to set a wall.
     * @param text the input variable
     */
    void setWall(string& text);

    /**
     * @brief Creates a new QuoridorGame
     * @param numPlayers the number of players.
     * @param size the size of the board.
     * @param names the names of the players.
     */
    QuoridorGame(unsigned numPlayers, int size, vector<string> &names);
public:

    /**
     * @brief Generates a new game.
     * @return the unique instance of the game.
     */
    static QuoridorGame & generateGame();

    /**
     * @brief Starts the game.
     */
    void play();

};


} //end of Quoridor
#endif // QUORIDORGAME

