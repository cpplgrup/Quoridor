#include "gameprinter.h"
#include "square.h"
#include "board.h"
#include <iomanip>

using namespace Quoridor;
using namespace std;


GamePrinter::GamePrinter() {
}

void GamePrinter::printBoard(ChangeEvent event)
{
    int row = 1;
    for (int col = 1; col <= event.getSource().getSize(); ++col) cout << setw(3)<< col;
    cout<<endl;
    for (vector<square> line: event.getSource().getBoard()) {
        for (square s: line) cout << "+" << (s.hasNeighbor(direction::north)? "  ": "--");
        cout << '+' << endl;
        for (square s: line) cout<< (s.hasNeighbor(direction::west)? ' ': '|')
              << (( event.getSource().getPlayerAtPosition(s.getPosition()) == -1)?"  " :"p"+to_string((event.getSource().getPlayerAtPosition(s.getPosition()) )+1));
        square s = line.back();
        cout << (s.hasNeighbor(direction::east)? ' ': '|');
        cout << ' ' << row << endl;
        ++row;
    }
    vector<square> line =  event.getSource().getBoard().back();
    for (square s: line) cout << '+' << (s.hasNeighbor(direction::south)? "  ": "--");
    cout << '+' << endl<<endl;
}

void GamePrinter::printCurrentPlayer(ChangeEvent event)
{
    cout << "Current player: " << event.getSource().getPlayer(event.getIndex()).getName() << " (p" << (event.getIndex()+1)<< ")"  << endl;
}

void GamePrinter::notifyChangePlayer(ChangeEvent event) {
    printCurrentPlayer(event);
}

void GamePrinter::printGame(ChangeEvent event) {
    cout << endl;
    printBoard(event);
    printCurrentPlayer(event);
    cout << endl;
}

void GamePrinter::notifyMovePlayer(ChangeEvent event) {
    cout << endl << "Player " << event.getSource().getPlayer(event.getIndex()).getName() << " (p" << (event.getIndex()+1)<< ") moved to postion (" << (event.getPosX()+1) <<','<<(event.getPosY()+1)<<")"<<endl << endl;
    printBoard(event);
}

void GamePrinter::notifyPlaceWall(ChangeEvent event) {
    cout << endl <<"Player " << event.getSource().getPlayer(event.getIndex()).getName() << " (p" << (event.getIndex()+1)<< ") placed wall in postion (" << (event.getPosX()+1) <<','<<(event.getPosY()+1)<<")"<<endl << endl;
    printBoard(event);
}
