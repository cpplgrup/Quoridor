include(../defaults.pri)

TEMPLATE = app

LIBS += -L../lib -llibView \
    -L../lib -llibModel



CONFIG += console

HEADERS += \
    gameprinter.h \
    promptutil.h \
    quoridorgame.h

SOURCES += \
    gameprinter.cpp \
    main.cpp \
    quoridorgame.cpp

CONFIG -= app_bundle
CONFIG -= qt
