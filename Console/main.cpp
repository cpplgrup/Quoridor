#include <iostream>
#include "position.h"
#include "player.h"
#include "board.h"
#include "observablequoridor.h"
#include "gameprinter.h"
#include "quoridorgame.h"



using namespace std;
using namespace Quoridor;

int main()
{
    cout << "Quoridor :p" << endl;

    QuoridorGame game = QuoridorGame::generateGame();
    game.play();

    return 0;
}

