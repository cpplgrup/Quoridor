/**
 * @file gameprinter.h
 * @brief Definition of class Quoridor::GamePrinter.
 */
#ifndef BOARDPRINTER
#define BOARDPRINTER

#include "quoridorobserver.h"
#include "board.h"
/**
 *@brief namespace for Quoridor
 */
namespace Quoridor {
/**
 * @brief The GamePrinter class is quoridor observer that defines how
 */
class GamePrinter :  public QuoridorObserver
{
private:
    void printBoard(ChangeEvent event);
public:
    /**
     * @brief Creates a GamePrinter
     */
    GamePrinter();

    /**
     * @brief Prints out game in desired format.
     * @param event change event.
     */
    void printGame(ChangeEvent event);

    /**
     * @brief Prints out the game and a line to notify players that a player have moved
     * @param event the change event.
     */
    void notifyMovePlayer(ChangeEvent event);

    /**
     * @brief Prints out the game and a line to notify players that a wall as been placed
     * @param event the change event.
     */
    void notifyPlaceWall(ChangeEvent event);

    /**
     * @brief Prints out the game and a line to notify players for current players
     * @param event the change event.
     */
    void printCurrentPlayer(ChangeEvent event);

    /**
     * @brief Prints out the game and a line to notify players that player have been changed
     * @param event the change event.
     */
    void notifyChangePlayer(ChangeEvent event);
};
}// end of Quoridor
#endif //BOARDPRINTER

