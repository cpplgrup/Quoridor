#ifndef PROMPTUTIL
#define PROMPTUTIL

#include <string>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <vector>
#include "position.h"
#include "direction.h"

using namespace std;
namespace Quoridor {

inline int getNumber(string & text) {
   bool isInt = false;
   int num;
   while( !isInt){
       try{ getline(cin,text);
           std::transform(text.begin(), text.end(), text.begin(), ::tolower);
           if (text == "exit" || text == "quit" || text == "stop" || text == "x") return -1;
          num =  stoi(text);
          isInt = true;
       } catch(invalid_argument & expt){
            cout <<"Not an integer... "<<endl << "Try again :";
       }
   }
   return num;
}

inline int parsePositive(string& text) {
    try {
        return stoi(text);
    } catch (invalid_argument & expt) {
        return -1;
    }
}

inline int promptBoardSize(string & text) {
    unsigned a = -1;
    cout << "Board size: ";
    a = getNumber(text);
    while (a < 5 || a > 19 || a % 2 != 1) {
        cout << "Incorrect (must be between 5 and 19 and uneven). Please try again: ";
        a = getNumber(text);
    }

    return a;
}

inline unsigned promptNbPlayers(string & text) {
   unsigned a = -1;
   cout << "Player count: ";
   a = getNumber(text);
   while (a != 2 && a != 4) {
       cout << "Incorrect (must be 2 or 4). Please try again: ";
       a = getNumber(text);
   }

   return a;
}

inline void setNames(string & text , unsigned numPlayers , vector<string>& names) {
    unsigned i= 1;
    while(i <= numPlayers){
        cout << "Name of player "<< i<< ": ";
        getline(cin,text);
        names.push_back(text);
        i++;
    }
}

 inline int promptPlayerChoice(string & text, int remainingWalls){
     cout << endl << "Please select :"<<endl;
     cout <<setw(5)<<"  1"<<" To move"<<endl;
     if (remainingWalls > 0) cout <<setw(5)<<"  2"<<" To place a wall (" << to_string(remainingWalls) << " remaining)" <<endl;
     cout << endl;
     int choice = getNumber(text);
     while((choice != 1 && choice != 2 && choice != -1) || (choice == 2 && remainingWalls == 0)) {
         cout << "Invalid choice";
         choice = getNumber(text);
     }
     return choice;
 }

 inline int promptX(string & text) {
     cout <<"X:  ";
     return getNumber(text);
 }

 inline int promptY(string & text) {
     cout <<"Y:  ";
     return getNumber(text);
 }

 inline const position *promptPos(string & text){
    cout << "position"<<endl;
    int x=promptX(text);
    if (x < 0) return nullptr;
    int y = promptY(text);
    if (y < 0) return nullptr;
    --x;
    --y;
    return new position(x,y);
 }

inline const direction promptOrientation(string& text) {
    cout << "Orientation (vertical/horizontal) : ";
    getline(cin, text);
    while (true) {
        std::transform(text.begin(), text.end(), text.begin(), ::tolower);
        if (text == "x" || text == "cancel") return direction::nodir;
        if (text == "v" || text == "vertical") return direction::north;
        if (text == "h" || text == "horizontal") return direction::west;
        cout << endl << "Incorrect input. Plase try again." << endl;
        cout << "Orientation (vertical/horizontal) : ";
        getline(cin, text);
    }
}

inline const direction promptDirection(string& text) {
    cout << "Enter direction : ";
    getline(cin, text);
    while (true) {
        std::transform(text.begin(), text.end(), text.begin(), ::tolower);
        if (text == "x" || text == "cancel") return direction::nodir;
        if (text == "up" || text == "north" || text == "above") return direction::north;
        if (text == "right" || text == "east") return direction::east;
        if (text == "down" || text == "south" || text == "below") return direction::south;
        if (text == "left" || text == "west") return direction::west;
        cout << endl << "Incorrect input. Plase try again." << endl;
        cout << "Enter direction : ";
        getline(cin, text);
    }
}

inline void printVictoryMsg(int playerIndex , string & name) {
    cout << "VICTORY !!!!! " << name << " (p"<<(playerIndex+1) <<") has won!"
         <<endl<<"The force is strong in this one.  Please celebrate accordingly, and may the force be with you young padawan!" <<endl <<endl;
    cout <<"Thanks for playing !! :)" << endl;
}

inline void printDrawMsg(int playerIndex , string & name) {
    cout << "DRAW!!!! "<<name << " (p"<<(playerIndex+1) <<") can't move nor place a wall anymore.."<<endl<<endl;
    cout <<"Thanks for playing !! :)" << endl;
}

inline void printQuitMsg(int playerIndex , string & name) {
    cout << "Player " << name << " (p"<<(playerIndex+1) <<") has quit the game!"<<endl<<endl;
    cout <<"Thanks for playing !! :)" << endl;
}

inline void printErrorMsg() {
    cout << "OOPS ! The game ended abnormally, we're sorry for that."<< "Please try again...."<<endl<<endl;
}
}//end of Quoridor
#endif // PROMPTUTIL

