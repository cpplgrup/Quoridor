#include "quoridorgame.h"
#include <string>
#include <stdexcept>
#include "promptutil.h"

using namespace Quoridor;
using namespace std;

QuoridorGame::QuoridorGame(unsigned numPlayers , int size, vector<string> &names): game(numPlayers , size , names)  ,output(new GamePrinter()) {
    game.registerObserver(output.get());
}

QuoridorGame & QuoridorGame::generateGame(){
    bool allIsWell = false;
    while(!allIsWell){
       try{
            string  text;
            int size =  promptBoardSize(text);
            unsigned numPlayers = promptNbPlayers(text);
            vector<string> names;
            setNames(text,numPlayers,names);
            static QuoridorGame instance(numPlayers,size,names);
            allIsWell = true;
            return instance;// Unique Instance
          }catch(range_error err){
            cout  << err.what() <<endl;
            cout  <<"Try again"<<endl<<endl;
        }
   }

   throw std::logic_error("Shouldn't happen. To remove compilation warning.");
}

void QuoridorGame::move(string &text)
{
    if (game.getBoard().canJump(game.getCurrentPlayer())) {
        cout << "jump possible, please enter ";
        const position *pos = promptPos(text);
        if (pos == nullptr) {
            game.setGameState(GameState::quit);
            return;
        }
        if (!game.movePlayer(*pos)) {
            cout << "Try again" << endl;
            move(text);
        }
    } else {
        direction dir = promptDirection(text);
        if (dir == direction::nodir) {
            game.setGameState(GameState::quit);
            return;
        }
        if (!game.movePlayer(dir)) {
            cout << "Try again" << endl;
            move(text);
        }
    }

}

void QuoridorGame::setWall(string &text) {
    cout << "Place wall : " << endl;
    cout << "In ";
    const position *pos = promptPos(text);
    if (pos == nullptr) return;
    direction dir = promptOrientation(text);
    if (dir == direction::nodir) return;
    if (!game.placeWall(*pos, dir)) {
        cout << "Try again"<<endl;
        setWall(text);
    }

    delete pos;
}

void QuoridorGame::makeChoice() {
    string text;
    int choice =  promptPlayerChoice(text, game.getBoard().getPlayer(game.getCurrentPlayer()).getNbWall());
    if(choice == 1) { // TODO: handle by enum?
        move(text);
    } else if (choice == 2) {
        setWall(text);
    } else {
        game.setGameState(GameState::quit);
    }

}

void QuoridorGame::endGame(int prevPlayer)
{
    string name = game.getBoard().getPlayer(prevPlayer).getName();
    switch (game.getGameState()) {
        case GameState::won:
            printVictoryMsg(prevPlayer ,name);
            break;
        case GameState::draw:
            printDrawMsg(prevPlayer ,name);
            break;
        case GameState::quit:
            printQuitMsg(prevPlayer, name);
            break;
        default:
            printErrorMsg();
            break;
    }
}

void QuoridorGame::play(){
    int prevPlayer;
    while(game.getGameState() == GameState::in_progress){
        prevPlayer = game.getCurrentPlayer();
        vector<position> moves;//Devrait etre global et utilisé pour afficher les coups possible
        game.getBoard().getPossibleMoves(prevPlayer,moves);
        if (game.getBoard().getPlayer(prevPlayer).hasWall() && moves.size() != 0){
            makeChoice();
        }else if (moves.size() != 0){
            string text;
            move(text);
        }else{
            string text;
            setWall(text);
        }
        moves.empty();
    }
    endGame(prevPlayer);
}
